const fs = require('fs');
const boardInfo = require('../callback1');

const boardsData = (callback) => {
    fs.readFile('../providedData/boards_1.json', 'utf8', (error, content) => {
        if (error) {
            callback(error, null);
        } else {
            const jsonData = JSON.parse(content);
            callback(null, jsonData);
        }
    });
};

boardsData((error, jsonData) => {
    if (error) {
        console.log(`Error occurred: ${error}`);
    } else {
        console.log(`The control is in the helper function`)
        boardInfo(jsonData, () => {
            console.log(`The control is again transferred to the helper function`)
        })
    }
});

module.exports = boardsData;