const fs = require('fs')
const listInfo = require('../callback2')

const boardsData = (callback) => {
    fs.readFile('../providedData/boards_1.json', 'utf8', (error, content) => {
        if (error) {
            callback(error, null);
        } else {
            const jsonData = JSON.parse(content);
            callback(null, jsonData);
        }
    });
};

boardsData((error, jsonData) => {
    if (error) {
        console.log(`Error occurred: ${error}`);
    } else {

        let fetchId = jsonData[1].id;
        listData(fetchId)
    }
});

const listData = (fetchId) => {

    fs.readFile('../providedData/lists_1.json', 'utf8', (error, content) => {
        if (error) {
            console.log(error)
        }
        else {
            console.log(`The control is in the helper function`)
            const jsonListData = JSON.parse(content)
            listInfo(fetchId, jsonListData, () => {
                console.log(`The control is again transferred to the helper function`)
            })
        }
    })
}

module.exports = listData;