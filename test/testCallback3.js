const fs = require('fs')
const cardInfo = require('../callback3')

const listsData = (callback) => {
    fs.readFile('../providedData/lists_1.json', 'utf8', (error, content) => {
        if (error) {
            callback(error, null);
        } else {
            const jsonData = JSON.parse(content);
            callback(null, jsonData);
        }
    });
};

listsData((error, jsonData) => {
    if (error) {
        console.log(`Error occurred: ${error}`);
    } else {

        let keys = Object.keys(jsonData)
        let fetchId = jsonData[keys[0]][1].id;

        cardData(fetchId)
    }
});

const cardData = (fetchId) => {

    fs.readFile('../providedData/cards_1.json', 'utf8', (error, content) => {
        if (error) {
            console.log(error)
        }
        else {
            console.log(`The control is in the helper function`)
            const jsonCardData = JSON.parse(content)
            cardInfo(fetchId, jsonCardData, () => {
                console.log(`The control is again transferred to the helper function`)
            })
        }
    })
}