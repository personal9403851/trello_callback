const fs = require('fs');
let thanosId = 'mcu453ed';

function execute() {

    const thanosFetchId = (callback) => {

        let thanosInnerId = [];

        fs.readFile('./providedData/lists_1.json', 'utf8', (error, content) => {
            if (error) {
                callback(error, null);
            } else {
                const jsonListData = JSON.parse(content);

                Object.keys(jsonListData).forEach((obj) => {
                    if (obj === thanosId) {
                        jsonListData[obj].forEach((element) => {
                            thanosInnerId.push(element.id);
                        });
                    }
                });
                callback(null, thanosInnerId);
            }
        });
    };

    const thanosInfo = (thanosInnerId, callback) => {

        fs.readFile('./providedData/cards_1.json', 'utf8', (error, content) => {
            if (error) {
                callback(error);
            } else {
                const jsonCardData = JSON.parse(content)
                console.log(`Information for Thanos boards:`)
                thanosInnerId.forEach(thanosId => {

                    Object.keys(jsonCardData).forEach((cardsId) => {

                        if (cardsId === thanosId)
                            console.log(jsonCardData[cardsId])
                    })
                })
                callback(jsonCardData);
            }
        })
    }

    const cardsForMindList = (jsonCardData) => {

        const MindId = 'qwsa221'
        console.log(`All cards for the Mind list are:`)

        Object.keys(jsonCardData).forEach((key) => {
            if (key === MindId)
                console.log(jsonCardData[key])
        })
    }

    thanosFetchId((error, thanosInnerId) => {
        if (error) {
            console.error(error);
        } else {
            thanosInfo(thanosInnerId, (callback) = (jsonCardData) => {
                cardsForMindList(jsonCardData);
            })
        }
    })
}

module.exports = execute;